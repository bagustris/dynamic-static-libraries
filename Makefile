
LIB:= ${PWD}/libs

all: main

main: main.cpp ${LIB}/liblib1.so ${LIB}/liblib2.a
	g++ -L${LIB} $< -o $@ -llib1 -llib2 -Wl,-rpath,${LIB}

${LIB}/liblib1.so: lib1.o ${LIB}
	g++ -shared -o $@ $< 

${LIB}/liblib2.a: lib2.o ${LIB}
	ar rcs $@ $<

%.o: %.cpp
	g++ -c -Wall -Werror -fpic $<

${LIB}:
	mkdir $@

.PHONY: clean test
clean:
	rm -rf main libs

test: main
	./main
