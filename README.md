Content
=======

Very simple example that creates a shared and a static library and link them with an executable. The libraries are in another directory intentionally and the use of rpath is needed. The idea was to make a MWE.
